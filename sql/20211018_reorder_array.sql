create or replace function reorder(data text[], indices integer[])
  returns text[]
  language sql
  immutable strict leakproof
as $function$
  with indexed as (
    select unnest(data) as data, unnest(indices) as i
  ),
  res as (
    select coalesce(array_agg(data order by i), ARRAY[]::text[]) r
    from indexed
  )
  select coalesce(r, ARRAY[]::text[])
  from res
  ;
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
select plan(4);

select is (reorder(null, null), null);
select is (reorder(ARRAY[]::text[], ARRAY[]::integer[]), ARRAY[]::text[]);
select is(reorder(ARRAY['C', 'D', 'E', 'F', 'G', 'H'], ARRAY[3, 0, 4, 1, 2, 5]),
          ARRAY['D', 'F', 'G', 'C', 'E', 'H']);
select is(reorder(ARRAY['C', 'D', 'E', 'F', 'G', 'H'], ARRAY[3, 1, 4, 2, 0, 5]),
          ARRAY['G', 'D', 'F', 'C', 'E', 'H']);
rollback;

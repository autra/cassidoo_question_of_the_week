\pset format unaligned
\set width 9
\set height 40
\set flag_height 20
\set wave_mag 4

select
  (
    case
      when s = :wave_mag + 1 then
        ' ⬤  '
      when s < :wave_mag + 1 then
        '    '
      else
        '  ❚ '
    end
  ) || string_agg(colors.color, '' order by colors.c)
from
  generate_series(1, :height) s,
  lateral (
    select
      c,
      case
        when (s between 6 + wave and 6 + :flag_height + wave) then
          case
            when c < :width then
              chr(27)|| '[48;2;228;3;3m '  ||chr(27)||'[0m'
            when c < :width * 2 then
              chr(27)|| '[48;2;255;140;0m '  ||chr(27)||'[0m'
            when c < :width * 3 then
              chr(27) || '[48;2;255;237;0m '  ||chr(27)||'[0m'
            when c < :width * 4 then
              chr(27) || '[48;2;0;128;38m '  ||chr(27)||'[0m'
            when c < :width * 5 then
              chr(27) || '[48;2;00;77;255m '  ||chr(27)||'[0m'
            when c < :width * 6 then
              chr(27)|| '[48;2;117;7;135m '  ||chr(27)||'[0m'
            else
              ''
          end
        else
          ' '
      end as color
    from generate_series(1, :width * 6) c,
    lateral round( :wave_mag * sin(2 * 3.1416 * c / (:width * 6))) as wave
  ) colors
  group by s
;


/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(1);
  -- too lazy to test
  select is(true, true);

rollback;

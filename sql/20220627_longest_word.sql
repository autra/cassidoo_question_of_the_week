CREATE OR REPLACE FUNCTION cassidoo.longest_word(word text, words text[])
 RETURNS text
 LANGUAGE sql
 IMMUTABLE STRICT SECURITY DEFINER
AS $function$
  select
    curr
  from
    unnest(words) with ordinality u(curr, idx),
    lateral (
      with recursive it as (
        select 1 as i, word, curr
        UNION
        select i+1, substr(word, pos+1, length(word)) as word, substr(curr, 2, length(curr))
        from
          it,
          lateral position(substr(curr, 1, 1) in word) pos
        where pos > 0 and curr != '' and word != ''
      )
      select curr = '' as is_match
      from it
      order by i desc limit 1
    ) _
  where is_match
  order by length(curr) desc, idx asc
  limit 1;
$function$;

/*
 * ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛
 */

begin;
select plan(9);

select is(cassidoo.longest_word(null, null), null);
select is(cassidoo.longest_word(null, ARRAY[]::text[]), null);
select is(cassidoo.longest_word('', null), null);
select is(cassidoo.longest_word('', ARRAY[]::text[]), null);
select is(cassidoo.longest_word('', ARRAY['foo']), null);
select is(cassidoo.longest_word('abppplee', ARRAY['out', 'dog', 'cactus', 'kangaroo']), null);
select is(cassidoo.longest_word('abppplee', ARRAY['able', 'ale', 'apple', 'bale', 'kangaroo']), 'apple');
select is(cassidoo.longest_word('abappplee', ARRAY['able', 'ale', 'bale', 'kangaroo']), 'able');
select is(cassidoo.longest_word( 'abappplee', ARRAY[ 'bale', 'able', 'ale', 'kangaroo']), 'bale');
rollback;

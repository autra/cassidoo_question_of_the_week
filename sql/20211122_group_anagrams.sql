create or replace function cassidoo.group_anagrams(input text[])
  returns table(str_group text[])
  language sql
  immutable strict
as $function$
  -- classify each string by a string composed with all its letters in
  -- alphabetic order
  with
  classified as (
    select
      input_str,
      -- group back exploded string with chars in orders
      string_agg(char, '' order by char) as class
    from
      -- give me a tables!
      unnest(input) input_str,
      -- explode string!
      regexp_split_to_table(input_str, '') char
    group by input_str
  )
  -- then just build arrays, grouping by the calculated class
  select
    array_agg(input_str order by input_str) as same_class
  from
    classified
  group by class
;
$function$;


/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
select plan(4);

select is_empty('select cassidoo.group_anagrams(null)');
select is_empty('select cassidoo.group_anagrams(array[]::text[])');
prepare chave as
  select cassidoo.group_anagrams(ARRAY['foo', 'bar', 'ofo', 'rab', 'oof', 'test', '', 'augustin']);
select set_eq(
  'chave',
  $$values
    (ARRAY['']),
    (array['augustin']),
    (array['test']),
    (array['bar', 'rab']),
    (array['foo', 'ofo', 'oof'])
  $$
);
prepare chave2 as
  select cassidoo.group_anagrams(ARRAY['eat', 'tea', 'ten', 'poop', 'net', 'ate']);
select set_eq(
  'chave2',
  $$values
    (array['poop']),
    (array['net', 'ten']),
    (array['ate', 'eat', 'tea'])
  $$
);

rollback;

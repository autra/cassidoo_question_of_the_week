CREATE OR REPLACE FUNCTION public.longuest_prefix(words text[])
RETURNS text
LANGUAGE sql
IMMUTABLE LEAKPROOF STRICT
AS $function$
with broken(by_idx, idx) as (
  select
    string_agg(c, '') by_idx,
    idx
  from
    unnest(words) as v(text),
    lateral string_to_table(text, NULL) with ordinality as s(c, idx)
  group by idx
),
matching(matching_char, previous_matched, idx) as (
  select
    common_char as matching_char,
    bool_and(common_char is not null) over (order by idx) as previous_matched,
    idx
  from
    broken,
    lateral (
      select (regexp_match(by_idx, '^(.)\1{' || cardinality(words) - 1 || '}$'))[1]
    ) as regex_res(common_char)
),
prefix(v) as (
  select
    string_agg(matching_char, '' order by idx) filter (where previous_matched)
  from
    matching
)
select
  coalesce(
    (select prefix.v from prefix),
    case when cardinality(words) > 0 then empty else null end
  )
from
  (values('')) as _(empty)
  ;
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

-- run this with pgtap extension activated!
-- or use pg_prove
select plan(11);

select is(longuest_prefix(null), null);
select is(longuest_prefix(ARRAY[]::text[]), null);
select is(longuest_prefix(array['foo']), 'foo');
select is(longuest_prefix(array['foo', 'bar']), '');
select is(longuest_prefix(array['foo1', 'foo2']), 'foo');
select is(longuest_prefix(array['foo1o', 'foo2o', 'foo3oo']), 'foo');
select is(longuest_prefix(array['pandemie', 'panflute', 'panpanpan']), 'pan');
select is(longuest_prefix(array['qwerty', 'qwertyuiop']), 'qwerty');
select is(longuest_prefix(array['cranberry','crawfish','crap']), 'cra');
select is(longuest_prefix(array['parrot', 'poodle', 'fish']), '');
select is(longuest_prefix(array[
                            'Xray',
                            'Xalam',
                            'Xanthoma',
                            'Xanthophyll',
                            'Xebec',
                            'Xenium',
                            'Xenomania',
                            'Xenophobia',
                            'Xiphias',
                            'Xiphodon',
                            'Xylograph',
                            'Xylophagia',
                            'Xylophone',
                            'Xyris']),
                          'X');

create or replace function public.maximum_profit(list_prices integer[])
  returns integer
  language sql
  immutable strict
  parallel safe
as $function$
  -- make it a table (easier in pure sql)
  -- keep the index
  with input as (
      select
        val, idx
      from
        unnest(list_prices)  with ordinality as _(val, idx)
  ),
  best as (
    select
        -- max of all the max per rows
        max(max_diff)
    from
        input i,
        -- for each row, we inspect all the following rows
        lateral (
            select
                -- for one row, keep the max
                max(other.val - i.val) max_diff
            from
                input other
            where other.idx > i.idx
        ) maxes
  )
  select greatest(max, 0) from best
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(8);
  select is(maximum_profit(array[]::integer[]), 0);
  select is(maximum_profit(array[1]), 0);
  select is(maximum_profit(array[4, 3, 2, 1]), 0);
  select is(maximum_profit(array[4, 3, 4, 1]), 1);
  select is(maximum_profit(array[7,1,5,3,6,4]), 5);
  select is(maximum_profit(array[1,7,5,3,6,4]), 6);
  select is(maximum_profit(array[1,7,5,3,6,16]), 15);
  select is(maximum_profit(array[7,6,16]), 10);
rollback;

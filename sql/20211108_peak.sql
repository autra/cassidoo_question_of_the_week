create or replace function cassidoo.local_peaks(input integer[])
  returns integer[]
  language sql
  immutable strict leakproof
as $function$
  with peaks as (
    select
      idx - 1 as idx,
      val >= lag(val, 1) over w and val >= lead(val, 1) over w
      as is_peak
    from unnest(input) with ordinality as i(val, idx)
    window w as (order by idx)
  )
  select coalesce(
    (
      select array_agg(idx) from peaks where is_peak
    ),
    array[]::integer[]
  );
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
select plan(5);

select is (cassidoo.local_peaks(null), null);
select is (cassidoo.local_peaks(array[]::integer[]), array[]::integer[]);
select is (cassidoo.local_peaks(array[1,2,3,1]), array[2]);
select is (cassidoo.local_peaks(array[1,3,2,3,5,6,4]), array[1, 5]);
select is (cassidoo.local_peaks(array[4,3,2,3,5,6,7]), array[]::integer[]);
rollback;

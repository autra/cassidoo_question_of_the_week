
create or replace function public.get_all_types()
  returns table(url text, name text)
  language sql
as $function$
  select
    value->'url'#>>'{}' as url,
    value->'name'#>>'{}' as name
  from
    http_get('https://pokeapi.co/api/v2/type/'),
    lateral jsonb_array_elements((content::jsonb)->'results')
$function$;

create or replace function public.pokemon_type_matchup(ptype text)
  returns text
  language sql
as $function$
  with response as (
    select status, content
    from http_get('https://pokeapi.co/api/v2/type/' || ptype)
  )
  select
    case when status = 200 then
      (select
        'Will get destroyed by ' || get_destroyed_against ||
        '. will destroy ' || destroy_against ||
        '. Is strong against ' || strong_against ||
        '. Is weak against ' || weak_against ||
        '. God against ' || god_against ||
        '. Useless against ' || useless_against || '.'
      from
          (select content::jsonb) as _(c)
          left join lateral (
            select coalesce(string_agg(t#>>'{}', ', '), 'none')
            from jsonb_path_query(c->'damage_relations'->'double_damage_from', '$.name'
          ) as _(t)) as a(get_destroyed_against) on true
          left join lateral (
            select coalesce(string_agg(t#>>'{}', ', '), 'none') from jsonb_path_query(c->'damage_relations'->'double_damage_to', '$.name') as _(t)
          ) as b(destroy_against) on true
          left join lateral (
            select coalesce(string_agg(t#>>'{}', ', '), 'none') from jsonb_path_query(c->'damage_relations'->'half_damage_from', '$.name') as _(t)
          ) as c(strong_against) on true
          left join lateral (
            select coalesce(string_agg(t#>>'{}', ', '), 'none') from jsonb_path_query(c->'damage_relations'->'half_damage_to', '$.name') as _(t)
          ) as d(weak_against) on true
          left join lateral (
            select coalesce(string_agg(t#>>'{}', ', '), 'none') from jsonb_path_query(c->'damage_relations'->'no_damage_from', '$.name') as _(t)
          ) as e(god_against) on true
          left join lateral (
            select coalesce(string_agg(t#>>'{}', ', '), 'none') from jsonb_path_query(c->'damage_relations'->'no_damage_to', '$.name') as _(t)
          ) as f(useless_against) on true
        )
      else
        'Not a valid pokemon'
    end as result
    from response
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(3);
  select is(pokemon_type_matchup('foo'), 'Not a valid pokemon');
  select is(pokemon_type_matchup('fighting'), 'Will get destroyed by flying, psychic, fairy. will destroy normal, rock, steel, ice, dark. Is strong against rock, bug, dark. Is weak against flying, poison, bug, psychic, fairy. God against none. Useless against ghost.');
  select is(pokemon_type_matchup('flying'), 'Will get destroyed by rock, electric, ice. will destroy fighting, bug, grass. Is strong against fighting, bug, grass. Is weak against rock, steel, electric. God against ground. Useless against none.');
rollback;

CREATE OR REPLACE FUNCTION public.string_product(n1 text, n2 text)
RETURNS text
LANGUAGE sql
IMMUTABLE LEAKPROOF STRICT
AS $function$
  -- d1 and d2 are data formatting
  -- from "123142", then present a table that looks like
  --
  --  d │ tenpow
  -- ═══╪════════
  --  1 │      5
  --  2 │      4
  --  3 │      3
  --  1 │      2
  --  4 │      1
  --  2 │      0
  with recursive d1 as (
    select
      d,
      10^(length(n1) - idx) as mult
    from
      string_to_table(n1, null) with ordinality s(d, idx)
  ),
  d2 as (
    select
      d,
      10^(length(n2) - idx) as mult
    from
      string_to_table(n2, null) with ordinality s(d, idx)
  ),
  -- prepare each elementary operation
  -- it's a cross join over all digits
  list as (
    select
      row_number() over () as i,
      d1.d as d1,
      d2.d as d2,
      d1.mult as mult1,
      d2.mult as mult2
    from d1, d2
  ),
  acc as (
    -- do the multiplication
    -- it's actually a reimplementaton of sum() aggregation function
    -- with a recursive CTE, as we can't use any builtin integer fn
    -- (if I understand the prerequisite correctly)
    select 1 as i, 0::bigint as acc
    union all
    -- I allow myself some elementary operations here
    -- it's possible to push this further, but I'm too lazy right now :-D
    select acc.i+1 as i, acc + d1::bigint * mult1::bigint * d2::bigint * mult2::bigint
    from acc
      join list on list.i = acc.i
  )
  select
    acc
  from acc
  order by i desc
  limit 1;
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
select plan(8);

select is(string_product(null::text, null::text), null);
select is(string_product('123', null), null);
select is(string_product(null, '456'), null);
select is(string_product('0', '0'), '0');
select is(string_product('123', '456'), '56088');
select is(string_product('987', '789'), '778743');
select is(string_product('1', '789'), '789');
select is(string_product('9999', '888888'), '8887991112');
rollback;

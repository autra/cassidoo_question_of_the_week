create or replace function public.num_balanced(input text)
  returns integer
  language sql
  immutable
  strict
  parallel safe
as $function$
  select
    abs(
      count(*) filter (where c = ')') - count(*) filter (where c = '(')
    )
  from
    string_to_table(input, null) c;
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(6);
  select is(num_balanced(null), null);
  select is(num_balanced('foo'), 0);
  select is(num_balanced('()'), 0);
  select is(num_balanced('(()'), 1);
  select is(num_balanced('))()))))()'), 6);
  select is(num_balanced(')))))'), 5);
rollback;

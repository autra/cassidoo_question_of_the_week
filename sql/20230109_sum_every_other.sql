create or replace function public.sum_every_other(input float)
  returns integer
  language sql
  immutable
  strict
  parallel safe
as $function$
  select
    coalesce(sum(v::int), 0)
  from (
    select
      row_number() over () as idx,
      v
    from
      string_to_table(input::text, null) with ordinality as char(v, idx)
    where v ~ '\d'
  ) _
  where idx % 2 = 0

$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(7);
  select is(sum_every_other(null), null);
  select is(sum_every_other(0), 0);
  select is(sum_every_other(1), 0);
  select is(sum_every_other(548915381), 26); -- 4+9+5+8
  select is(sum_every_other(10), 0);
  select is (sum_every_other(1010.11), 1); -- 0+0+1
  select is (sum_every_other(1010.12), 2); -- 0+0+1

rollback;

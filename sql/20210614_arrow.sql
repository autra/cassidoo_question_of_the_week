/* Given a direction and a number of columns, write a function that outputs an arrow of asterisks (see the pattern in the examples below)!
*
* Example:
*
* ```
* $ printArrow('right', 3)
* Output:
* *
*  *
*   *
*  *
* *
*
* $ printArrow('left', 5)
* Output:
*     *
*    *
*   *
*  *
* *
*  *
*   *
*    *
*     *
* ``` */
CREATE OR REPLACE FUNCTION public.print_arrow(direction text, columnnumber integer)
 RETURNS text
 LANGUAGE sql
 IMMUTABLE
AS $function$
  select
    string_agg(repeat(' ', s) || '*', E'\n')
  from (
    select
      case
        when direction = 'right' then columnnumber - abs(line  - columnnumber) - 1
        when direction = 'left' then abs(line - columnnumber)
        else /*uh oh!*/ 0
      end as s
    from
      generate_series(1, 2 * columnnumber - 1) line
  ) spaces;
$function$;



/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(1);
  select is(print_arrow('right', 10),
'*
 *
  *
   *
    *
     *
      *
       *
        *
         *
        *
       *
      *
     *
    *
   *
  *
 *
*');
rollback;

/* **Write a function that draws an ASCII art cube of given height x.**
 *
 * Example:
 *
 * $ drawCube(2)
 *   +----+
 *  /    /|
 * +----+ |
 * |    | +
 * |    |/
 * +----+
 *
 * $ drawCube(4)
 *
 *    +--------+
 *   /        /|
 *  /        / |
 * +--------+  |
 * |        |  |
 * |        |  +
 * |        | /
 * |        |/
 * +--------+
 */
create or replace function public.draw_cube(height integer)
  returns text
  language sql
  immutable
as $function$
  -- generate one line for each line of output, and calculate some preliminary infos
  with infos as (
    select
      line_number,
      -- the number of spaces we need before (for the top face)
      greatest(height / 2 - line_number + 2, 0)
        as space_prefix,
      -- the number of spaces we need to draw the right/east face
      -- it's an inverted, shifted, translated abs(x) :-)
      -- we do want the ::float here, otherwise it's an integer division and it
      -- goes down too soon
      floor(
        least(height / 2, (3 * height)::float / 4 - abs(line_number - 2 - (3 * height)::float / 4))
      )::integer
        as space_right_face,
      -- are we drawing an horizontal line?
      line_number = 1 or line_number = height / 2 + 2 or line_number = 3 * height / 2 + 3
        as is_horizontal,
      -- we start drawing bottom_line after one face + 2 horizontal lines
      line_number > height + 2
        as drawing_bottom_face
    from
      -- the number of lines: height + height / 2 for the top face + 3 horizontal edges
      generate_series(1, 3 * height / 2 + 3 ) line_number
  ),
  -- draw the cube
  cube_lines as (
    select
      -- we keep the line number because SQL doesn't guarantee the order of lines ;-)
      line_number,
      -- space prefix, might be ''
      repeat(' ', space_prefix) ||
      case
        -- horizontal line
        when is_horizontal then
          '+' || repeat('-', 2 * height) || '+'
        else
          -- front facing square edge, see lateral below
          front_facing_side_char || repeat(' ', 2 * height) || front_facing_side_char
      end
      ||
      -- now drawing the right edge of the east face
      case
        when space_right_face >= 0 then
          -- spaces
          repeat(' ', space_right_face) ||
          -- right edge
          case
            when drawing_bottom_face then
            '/'
            else case when line_number - 2 = height then '+' else '|' end
        end
        else '' -- we need this empty string, because 'foo' || null is null!
      end as line
    from
      infos,
      -- factorize top face char calculation
      lateral (select case when space_prefix > 0 then '/' else '|' end) as _(front_facing_side_char)
  )
  -- aggregate in one big string separated by \n
  select
    string_agg(line, E'\n' order by line_number)
  from cube_lines
  ;
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(1);
  select is(draw_cube(10),
'      +--------------------+
     /                    /|
    /                    / |
   /                    /  |
  /                    /   |
 /                    /    |
+--------------------+     |
|                    |     |
|                    |     |
|                    |     |
|                    |     |
|                    |     +
|                    |    /
|                    |   /
|                    |  /
|                    | /
|                    |/
+--------------------+');
rollback;

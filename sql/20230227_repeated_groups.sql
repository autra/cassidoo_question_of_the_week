create or replace function public.repeated_groups(input integer[])
  returns setof integer[]
  language sql
  immutable
  strict
  parallel safe
as $function$
  with marker as (
    select
      i,
      n,
      -- diff with previous one. If it's positive, it has changed!
      n - lag(n, 1) over (order by i) changed
    from
      unnest(input) with ordinality as f(n, i)
  ),
  grouped as (
    select
      n,
      -- sum over the absolute value of diff, so it increases each time the number changes
      -- we have created a group id :-)
      coalesce(sum(abs(changed)) over (order by i), 0) id
    from marker
  )
  -- we keep only the groups with more than 1 members
  select
    array_agg(n)
  from grouped
  group by n, id having count(*) > 1
  order by id;
$function$;

/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛ */

begin;
  select plan(4);
  select is_empty('select * from public.repeated_groups(null)');
  select is_empty(' select public.repeated_groups(ARRAY[]::integer[])');
  select is_empty('select * from public.repeated_groups(ARRAY[1, 2, 3, 4])');
  select results_eq(
    'select * from public.repeated_groups(ARRAY[1, 1, 0, 0, 8, 4, 4, 4, 3, 2, 1, 9, 9])',
    $$values(array[1, 1]), (array[0, 0]), (array[4, 4, 4]), (array[9, 9])$$
  );
rollback;

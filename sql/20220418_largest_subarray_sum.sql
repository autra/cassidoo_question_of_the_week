create or replace function cassidoo.largest_subarray_sum(the_array int[], n int)
  returns int[]
  language sql
  immutable strict
as $function$
  -- we know that no args are null at this point because of the strict mode
  -- so the special cases are only n=0 and empty array
  select array[]::int[]
  where n = 0 or the_array = array[]::int[]

  UNION ALL

  -- actual calculation
  (
    with subarray_sum(sum, result) as (
      select
        -- calculate each sum array sum
        sum(val) over w as sum,
        -- and keep the current subarray
        array_agg(val) over w as result
      from
        unnest(the_array) with ordinality as arr(val, idx)
      -- considering n elements starting from this row
      window w as (
        order by idx
        rows between current row and greatest(n-1, 0) following
      )
    )
    select result
    from
      subarray_sum
    where n > 0
    order by sum desc
    limit 1
  );

$function$;


/* ╺┳╸┏━╸┏━┓╺┳╸┏━┓
 *  ┃ ┣╸ ┗━┓ ┃ ┗━┓
 *  ╹ ┗━╸┗━┛ ╹ ┗━┛
 */

begin;
select plan(21);

-- implementation choices (by order of precedence):
-- - any null input gives null result
-- - empty array returns always empty array
-- - n=0 returns empty array
-- - n > array length retuns the whole array
select is(cassidoo.largest_subarray_sum(null::int[], null::int), null);
select is(cassidoo.largest_subarray_sum(null, 0), null);
select is(cassidoo.largest_subarray_sum(null, 1), null);
select is(cassidoo.largest_subarray_sum(ARRAY[]::int[], null), null);
select is(cassidoo.largest_subarray_sum(ARRAY[]::int[], 0), array[]::int[]);
select is(cassidoo.largest_subarray_sum(ARRAY[]::int[], 1), array[]::int[]);
select is(cassidoo.largest_subarray_sum(ARRAY[1], null), null);
select is(cassidoo.largest_subarray_sum(ARRAY[1], 0), array[]::int[]);
select is(cassidoo.largest_subarray_sum(ARRAY[1], 1), array[1]);
select is(cassidoo.largest_subarray_sum(ARRAY[1], 2), array[1]);
select is(cassidoo.largest_subarray_sum(ARRAY[1], 999), array[1]);
select is(cassidoo.largest_subarray_sum(ARRAY[1,2,3,4], 2), array[3,4]);
select is(cassidoo.largest_subarray_sum(ARRAY[1,2,3,4], 3), array[2,3,4]);
select is(cassidoo.largest_subarray_sum(ARRAY[4,3,2,1], 1), array[4]);
select is(cassidoo.largest_subarray_sum(ARRAY[4,3,2,1], 2), array[4,3]);
select is(cassidoo.largest_subarray_sum(ARRAY[4,3,2,1], 3), array[4,3,2]);
select is(cassidoo.largest_subarray_sum(ARRAY[3,1,4,1,5,9,2,6], 3), array[9,2,6]);
select is(cassidoo.largest_subarray_sum(ARRAY[3,1,4,1,5,9,2,6], 1), array[9]);
select is(cassidoo.largest_subarray_sum(ARRAY[3,1,4,1,5,9,2,6], 2), array[5,9]);
select is(cassidoo.largest_subarray_sum(ARRAY[3,1,4,1,5,9,2,6], 4), array[5,9,2,6]);
select is(cassidoo.largest_subarray_sum(ARRAY[3,1,4,1,5,9,2,6], 40), array[3,1,4,1,5,9,2,6]);

rollback;

/// This week's question:
/// Given a string s, you are allowed to delete at most k characters. Find if the string can be a palindrome after deleting at most k characters.
///
/// Example:
///
/// > kPal('abcweca', 2)
/// > true
///
/// > kPal('acxcb', 1)
/// > false
fn kpal(input: &str, k: u32) -> bool {
  let l = input.len();
  if l <= 1 {
    return true;
  }
  if input[0..1] == input[l-1..l] {
    return kpal(&input[1..l - 1], k);
  } else {
    if k == 0 {
      return false;
    } else {
      return kpal(&input[0..l - 1], k-1) || kpal(&input[1..l], k - 1);
    }
  }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_kpal() {

      assert_eq!(kpal("radar", 0), true);
      assert_eq!(kpal("radaro", 0), false);
      assert_eq!(kpal("radaro", 1), true);
      assert_eq!(kpal("abcweca", 1), false);
      assert_eq!(kpal("abcweca", 2), true);
      assert_eq!(kpal("abcweca", 3), true);
      assert_eq!(kpal("abcweca", 4), true);
      assert_eq!(kpal("abcweca", 5), true);
      assert_eq!(kpal("abcweca", 6), true);
      assert_eq!(kpal("abcweca", 7), true);
      assert_eq!(kpal("acxcb", 1), false);
      assert_eq!(kpal("acxcb", 2), true);
      assert_eq!(kpal("acxcb", 3), true);
      assert_eq!(kpal("acxcb", 4), true);
      assert_eq!(kpal("foobarbaz", 0), false);
      assert_eq!(kpal("foobarbaz", 1), false);
      assert_eq!(kpal("foobarbaz", 2), false);
      assert_eq!(kpal("foobarbaz", 3), false);
      assert_eq!(kpal("foobarbaz", 4), false);
      assert_eq!(kpal("foobarbaz", 5), false);
      assert_eq!(kpal("foobarbaz", 6), true);

    }
}

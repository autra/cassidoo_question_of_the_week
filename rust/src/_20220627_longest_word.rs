/* The coding problem:
 *
 * Given a string str and a set of words dict, find the longest word in dict that is a subsequence of str.
 *
 * Example:
 *
 * ```rust
 * let str = "abppplee"
 * let dict = {"able", "ale", "apple", "bale", "kangaroo"}
 *
 * $ longestWord(str, dict)
 * $ 'apple'
 * // "able" and "ale" also work, but are shorter than "apple"
 * // "bale" has all the right letters, but not in the right order
 * ```
 *
 * */

fn longest_word<'a, 'b>(word: &'a str, words: &[&'b str]) -> Option<&'b str> {
  if words.len() == 0 {
    return None;
  }

  let mut biggestw: Option<&str> = None;
  let word_char_count = word.chars().count();
  for w in words {
    let mut word_pointer = 0;
    let mut curr_w_it = w.chars();
    let contained;
    // iterate over chars
    loop {
    //for curr_c in curr_w_it {
        let curr_c = curr_w_it.next();
        match curr_c {
            None => {
                // we reached the end of curr, without having reached the end of word
                // we found a word!
                contained = true;
                break;
            },
            Some(curr_char) => {
                // still some words to get
                // stop condition, we reached the end of word
                if word_pointer == word_char_count {
                    contained = false;
                    break;
                }
                let mut found = false;
                // advance through word
                while !found && word_pointer < word_char_count {
                    if word.chars().nth(word_pointer).unwrap() == curr_char {
                        found = true;
                    }
                    word_pointer += 1;
                }
            }
        }
    }
    if contained {
        match biggestw {
            None => {
                biggestw = Some(w);
            },
            Some(biggestw_real) => {
                if w.chars().count() > biggestw_real.chars().count() {
                    biggestw = Some(w);
                }
            }
        }
    }
  }

  return biggestw;
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_longest_word() {
      assert_eq!(longest_word("", &[]), None);
      assert_eq!(longest_word("", &[""]), Some(""));
      // no word is contained in input
      assert_eq!(longest_word("abppplee",
                             &[
                                "out",
                                "dog",
                                "cactus",
                                "kangaroo"
                             ]), None);
      assert_eq!(longest_word("abppplee",
                             &[
                                "able",
                                "ale",
                                "apple",
                                "bale",
                                "kangaroo"
                             ]), Some("apple"));
      // next 2 tests: if 2 words matches, the first one is returned
      assert_eq!(longest_word("abappplee",
                             &[
                                "able",
                                "ale",
                                "bale",
                                "kangaroo"
                             ]), Some("able"));
      assert_eq!(longest_word("abappplee",
                             &[
                                "bale",
                                "able",
                                "ale",
                                "kangaroo"
                             ]), Some("bale"));
    }

}

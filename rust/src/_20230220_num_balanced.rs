/* Given a string of parenthesis, return the number of parenthesis you need to add to the string in order for it to be balanced.
 *
 * Examples:
 *
 * > numBalanced(`()`)
 * > 0
 *
 * > numBalanced(`(()`)
 * > 1
 *
 * > numBalanced(`))()))))()`)
 * > 6
 *
 * > numBalanced(`)))))`)
 * > 5
 *
 **/

fn num_balanced(input: &str) -> u32 {
  let mut acc: i32 = 0;
  for char in input.chars() {
    match char  {
      '(' => acc += 1,
      ')' => acc -= 1,
      _ => ()
    }
  }
  println!("{}", acc);
  return acc.abs() as u32;
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_longest_word() {

      assert_eq!(num_balanced("()"), 0);
      assert_eq!(num_balanced("(()"), 1);
      assert_eq!(num_balanced("))()))))()"), 6);
      assert_eq!(num_balanced(")))))"), 5);
    }
}
